## GitLab Feature/Vendor Implementation Privacy Review

This assessment should be performed prior to the following:

1. a feature release that involves new collection or processing of personal data.
2. new software tool implementation
3. new uses for existing software tools

For additional information about Privacy Review requirements, please consult the:

* [DPIA FAQs](https://about.gitlab.com/handbook/legal/global-compliance/#data-protection-impact-assessment-frequently-asked-questions-dpia-faq)
* [Data Protection Impact Assessment Policy](https://about.gitlab.com/handbook/engineering/security/dpia-policy/)

## Requestor to Complete

### Name Issue

- [ ] Name the issue as follows: Feature/Product Name - Privacy Review - Business Owner/PM's Name

### Link Issues

- [ ] Link to any relevant issues that relate to the new tool/feature and to the Security Review issue to this issue (link in "Related Issues" box, not in issue description or comments)

**Please note the following definitions:
* For GitLab feature review, "user" = a free user, a paid customer, a customer's end users
* For new vendor tool implementation, "user" = a GitLab team member 
* "Third party" = any individual or company outside of GitLab or immediate vendor. This includes independent contractors working with GitLab-controlled personal data.

### Product Details

1. **Targeted Release/Implementation Date:**

2. **Provide a description of the Project, including key features.** 

3. **What is the business use case and/or customer need for this feature?**

### New Data Collection and Use
4.  **Will any *new* personal data be collected?**

<details>
<summary>**If answer is *yes*, please indicate personal data type and use/processing in the table below.**</summary>

*Examples of personal data types: name, email address, physical address, phone number, SSN or Gov. ID Number, genomic data, biometric data, race or ethnic origin, religion, affiliations (e.g. trade, union, or political), health data, sexual orientation, gender identity, IP address, location, unique identifiers (e.g. cookies, mobile device identifier)

| **Personal Data Type** | **Source of Personal Data** | **Description of Use/Processing for Personal Data**  |
| --- | --- | --- |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
  
</details>

### New Data Storage 
5. **Is the new personal data being stored outside of GitLab's AWS or Google Cloud infrastructure**? 

5a. **If not, where will new personal data be stored**?

6. **Will personal data be encrypted in transit and at rest**?

7. **Is any new personal data to be shared with third parties**?

<details>
<summary>**If answer is *yes*, please indicate personal data type and justification for sharing**.</summary>

| **Personal Data Type** | **Shared with...** | **Justification for Sharing**  |
| --- | --- | --- |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
  
</details> 

### Existing Data Use
8.  **Will any existing personal data be used or processed for a new purpose?**

<details>
<summary>**If answer is *yes*, please indicate personal data type and use/processing in the table below.**</summary>

*Examples of personal data types: name, email address, physical address, phone number, SSN or Gov. ID Number, genomic data, biometric data, race or ethnic origin, religion, affiliations (e.g. trade, union, or political), health data, sexual orientation, gender identity, IP address, location, unique identifiers (e.g. cookies, mobile device identifier)

| **Personal Data Type** | **Source of Personal Data** | **Description of Use/Processing for Personal Data**  |
| --- | --- | --- |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
  
</details>

### Existing Data Storage 
9. **Is the existing personal data being stored outside of GitLab's AWS or Google Cloud infrastructure**? 

9a. **If so, where is existing personal data stored**?

9b. **Will existing personal data be copied to a new repository**?

10. **Is the existing personal data encrypted in transit and at rest**?

11. **Is any existing personal data going to be shared with third parties as a result of new feature/tool/use**?

<details>
<summary>**If answer is *yes*, please indicate personal data type and justification for sharing in the table below.**</summary>

| **Personal Data Type** | **Shared with...** | **Justification for Sharing**  |
| --- | --- | --- |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |
  
</details> 

### Processing Data
12. **Are any new algorithms applied to new or existing personal data that don't involve human involvement**?

---
---

## Privacy Officer to Complete 
### Risks and Mitigation

| **Risk**   | **Valid Processing Mechanism** | **Mitigation** |
| ------ | ------ | ------ | 
| describe risk | processing mechanism | describe mitigation |
| describe risk | processing mechanism | describe mitigation | 

1. DPA Required?
	- [ ]  N/A
	- [ ]  Gitlab DPA
	- [ ]  Vendor DPA

### Approval

- [ ]  Yes, with above mediation in place. 
- [ ]  No

---
---

## For Requestor to Complete
**Comment below with (1) acknowledgement of mitigation plans and (2) deadline commitment for implementation of mitigation.

---
---

/confidential
/label ~"Privacy Vend Rev::Intake"
