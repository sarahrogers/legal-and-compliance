**Initial Issue template for global expansion legal process**

_This template should be used to start and manage the legal process of gathering, processing and assessing information when we look at opening a new country for hire/global expansion_

*Welcome* to the legal global expansion process issue! Global expansion process for Legal is set out [here](https://docs.google.com/document/d/1pw5ji3IuAgUiMIJLNPIOU_ADNHtYrAN0iOKXxJjlcAk/edit)

On receiving instruction to move ahead with an assessment of legal risk for opening a country for hire (or expanding or scaling in a country), a folder will be created for that company in the shared [Global expansion drive](https://drive.google.com/drive/folders/0ALzUKh4XsBAsUk9PVA?usp_dm=true) and an issue opened.

**Issue Creation Steps**
1. * [ ] Appropriately title the issue according to the country being assessed using this format: Global expansion - legal process - COUNTRY NAME
2. * [ ] Please mark this issue as confidential and assign to:
* [ ] - Emily Plotkin and Sarah Rogers
* [ ] - Darren Burr (EMEA) OR Tara Kumpf (APAC) (depending on the location of the country)
* [ ] - Rashmi Chachra
* [ ] - Lynsey Sayers
* [ ] - Matthew Taylor
* [ ] - Harley Devlin
* [ ] - Robin Schulman?
3. * [ ] Add appropriate labels [tbc]
4. * [ ] AAdd a due date ?

**Preliminary step where we already have a presence in the country:**
Gather current status information with [Employment Solutions Partner](@hdevlin) which can mostly be gathered from the [Country list (current hiring)](https://docs.google.com/spreadsheets/d/1rH8Afcnp38knsnchJtm2iPAzCSO8qxsum-fkdNFspRE/edit#gid=1223524307), the [Current database & road map for Business Expansion](https://docs.google.com/spreadsheets/d/1eotWhWgMZLuoUjLtv2aqYf28uwxrjFnfEGp3n9RxVR0/edit#gid=0) and the [Global Expansion Strategy - Int'l Expansion Team Considerations](https://docs.google.com/spreadsheets/d/1L8A_tUIo2x6Kf-H01Rm6zcU1lfWB1y01puxZl6jwMzI/edit#gid=0) and inputted to a new copy of [this document](https://docs.google.com/document/d/1QPulaTO9R1qISFM9uAL1LHybQaqhukgmJgk0j-5vDKA/edit)

## Initial Steps for Legal
### 1. Inform CLO Staff
* [ ] Send an initial email to each legal team director (with a link to this issue so they may follow progress and provide feedback) requesting information on the following:

* [ ] **Rashmi Chachra - Corporate**
1. Setup costs and requirements
- Including registration requirements 
- Including any social fund requirements
2. Choice of entity

3. Board membership requirements
- Including board member residency requirements, if any, for the jurisdiction
4. Equity
- Including ESPP
5. Query whether want an estimate collected from counsel for review of corporate needs to move into new location and if yes, seek confirmation on any specific questions to be included in an estimate request 

* [ ] **Lynsey Sayers - Product and Privacy**
1. Privacy Implications
- Customers
- Team members (How many team members do we plan to add in the next year to this location?)
2. Intellectual Property
- Customers
- Team members
3. Import/Export Compliance

4. Query whether want an estimate collected from counsel for review of product and privacy needs to move into new location and if yes, seek confirmation on any specific questions to be included in an estimate request 

* [ ] **Matthew Taylor - commercial**
1. Effect on FedRamp
2. Address Public Sector needs in location
3. Downstream commercial implications
4. Query whether want an estimate collected from counsel for review of commercial needs when moving into new location and if yes, seek confirmation on any specific questions to be included in an estimate request

* [ ] **Emily Plotkin - Employment**
1. PEO v IC matric
2. Expansion Matrix
3. Custom 

### 2. Engage Counsel
* [ ] Engage counsel with presence in location.  Start with Baker McKenzie or Osborne Clarke, both of whom are already engaged with GitLab and have large footprints that generally match needs. [Bakers use a similar structure/template for each country - consider what we need]
    * [ ] 1. If these firms do not have a footprint in the location, ask CLO-Staff for recommendations 
    * [ ] 2. Conduct introductions and request cost estimates _[Do/should we have a template?] _ * [ ][Save estimates etc to country folder for good matter management]
      * [ ]  - For counsel on recommended employment solution, including how equity is handled in the location
      * [ ]  - For information regarding employment compliance requirements in location (no matter what employment solution)
      * [ ]  - For cost of employment contract template creation if choose branch or entity
      * [ ]  - For review of current handbook and localization (as needed) of policies if choose branch or entity
      * [ ]  - For cost of equity review and setup
      * [ ]  - For cost of corporate setup support if choose branch or entity
      * [ ]  - Cost estimates for initial product and privacy advice
      * [ ]  - Cost estimates for initial commercial advice
  * [ ]   3. Get engagement letter based on what we actually agree to in Section b above [save engagement letters etc to country folder for good matter management] [add link to template issue for signature]
  * [ ]   4. Track costs for accruals in [this spreadsheet](https://docs.google.com/spreadsheets/d/1Gpzik5UJUck9mDK3etL92xJbNQRkkLawgqyr87_A07E/edit#gid=0) 

## Research and Discovery

* [ ] Assess advice from counsel
Follow up with:
* [ ] - Emily Plotkin and 
* [ ] - Darren Burr (EMEA) OR Tara Kumpf (APAC) (depending on the location of the country)
* [ ] - Rashmi Chachra
* [ ] - Lynsey Sayers
* [ ] - Matthew Taylor
to ensure advice from counsel has been received and reviewed

## Assessment/Decision-Making

### A. First step: What is the recommended employment solution to begin with?

_i. PEO/Employer of Record_

1. If considering PEO/Employer of Record, do the laws of the jurisdiction recognize this type of employment solution? 
2. Is there a threshold number of employees employed by PEO/EOR before the company should move to a different solution?
3. Does a PEO support equity in this location?
4. Does a PEO support all GitLab roles in this location (Sales, including PubSec, R&D, Support, G&A, Marketing)
5. Is this a location where we are trying to reach Public Sector clients?  If so, can a PEO support PubSec?
6. Time to set up PEO in location

_ii. Branch_

1. Is there a threshold number of employees for this option?
2. Are there types of employment roles (Sales, including PubSec, R&D, Support, G&A, Marketing) that this option does not support?
3. Can a branch support a Public Sector role?
4. What is the risk of permanent establishment?
5. Can a branch support equity?  What effect does it have on the foreign entity serving as the employer?
6. What does the foreign entity need to do to register to employ directly in the location?
7. Are the compliance requirements the same as if the company had a direct entity in this location?
8. Costs of a branch
9. Time to set up branch in location

_iii. Entity/Subsidiary_

1. Reasoning for moving directly to entity
2. Compliance requirements (see below)
3. Equity implications
4. Costs of creation
5. Time to set up entity

### B. Next Step (if PEO/EOR)

_i. If PEO/EOR, then for Employment:_

1. What PEO can support this location?
2. What are the co-employment risks in this location?
3. What will the relationship be between the PEO and the team member?  

    a. Will it be serving as the Employer of Record where the team member is a direct employee of the PEO?

    b. Will there be a consulting relationship between the PEO and the team member?

    c. Temporary work agency model?

    d. Fixed term contract or indefinite term contracts?

4. What is covered in the services agreement between GitLab and the PEO?
5. Does the PEO have its own entity in country or is it working with a partner?
6. How does the PEO support equity?
7. How does the PEO support sales commission plans?
8. Can the PEO support all of GitLab’s roles (e.g., sales, engineering, G&A, marketing)

    a. Can a PEO support a Public Sector role?  Will we be desiring a Public Sector role in this location?

9. How does the PEO support GitLab’s IP?  Does it want to review our PIAA? 
10. What benefits does the PEO provide?  How do those relate to the benefits currently being offered team members in this location?
11. Are there different levels of employment in this location? Can the PEO support all levels? Are there any restrictions/implications with said levels? Benefit differences? ER statutory differences? Costs?
12. Contract Duration (Definite/indefinite) ie. Hungary has definite contracts with limitations (5 yrs max)
13. What services does the PEO provide?

    a. Employment Contract template creation?

    b. Background check services?

    c. Onboarding services?

    d. Human Resources services?

    e. Time tracking services?

    f. Policy creation? (including how these policies correlate with GitLab’s policies)

    g. Offboarding services?

    h. Costs per employee for use of the PEO (Are the services noted above included in the cost or extra?)

    i. Other services provided?  Costs of such services?

14. How long does it usually take to hire in this location?

    a. Are there particular months during the year that are difficult for hire?  (e.g., August if vacations)

    b. What type of notice period is typical to know when to develop pipeline
 
15. Is there a threshold number of team members in the location where the company should consider moving to a branch or entity?

16. Coordinate with Talent Acquisition on roles to be hired in location and how they will be making offers

_ii. If PEO then for Corporate_

1. Coordinate with Employment team on Equity setup

_iii. If PEO then for Product and Privacy_

1. Review employment privacy requirements for PEO team members

2. Review any import/export requirements regarding that the company will have team members in this location

_iv. If PEO then for Commercial_

1. Review commercial implications of team members in location (no team member in location should be signing contracts on behalf of company)

2. Any commercial implications or effect on FedRamp for team members in location?

3. Can PubSec be supported via PEO?

### B. Next Step (if Branch)

_i. If Branch, then for Employment_

1. What payroll providers exist and what are the downstream implications on our payroll team?

2. What services will the payroll providers provide?

3. Total Rewards

4. Are there pension or other statutory benefit requirements?

    a. What additional benefits can the company provide?

    b. How does that compare to what the team members currently have and what is required in market? 

    c. If overtime must be included can we include it in the base pay in the first place?

4. What type of employment contracts do we need and will counsel provide them?

    a. What employment terms and conditions must be included in the contract?

    - Probationary periods
    - Applicable collective bargaining agreements
    - Work rules
    - Required terms
    - Time tracking and/or overtime and what processed need to be in place
        - If there is overtime, can it be included in the base salary offered?
    - Information on notice periods at end of employment
    - Process for dismissals
        - Severance payments
        - Redundancy process and/or payments
5. What additional policies do we need or supplements to our global policies?
6. Additional compliance
7. Background checks
8. Privacy
9. If we have current team members in the location, what do we need to do to convert them to branch employees?

_ii. If Branch then for Corporate_

1. Branch of which subsidiary?
2. Coordinate with Employment team on Equity setup

_iii. If Branch then for Product and Privacy_

1. Review employment privacy requirements for direct employees of a branch
2. Review any import/export requirements regarding that the company will have team members in this location

_iv. If Branch then for Commercial_

1. Review commercial implications of team members who are direct employees of a branch of a subsidiary in location (no team member in location should be signing contracts on behalf of company)
2. Any commercial implications or effect on FedRamp for team members in location?
3. Can PubSec be supported via branch of a foreign entity?

### B. Next Step (if Entity/Subsidiary)

_i. If Entity/Subsidiary then for Employment_

1. What employment terms and conditions must be included in the contract?

    a. Probationary periods

    b. Applicable collective bargaining agreements

    c. Work rules

    d. Required terms

    e. Time tracking and/or overtime and what processes need to be in place

    - If there is overtime, can it be included in the base salary offered?

    f. Information on notice periods at end of employment

    g. Process for dismissals

    - Severance payments

    - Redundancy process and/or payments

2. What payroll providers exist and what are the downstream implications on our payroll team?
3. What services will the payroll providers provide?
4. Total Rewards
a. Are there pension or other statutory benefit requirements?
b. What additional benefits can the company provide?
c. How does that compare to what the team members currently have and what is required in market? 
d. If overtime must be included can we include it in the base pay in the first place?

5. What additional policies do we need or supplements to our global policies?
6. Additional compliance
7. Background checks
8. Privacy
9. If we have current team members in the location, what do we need to do to convert them to branch employees?
10. Sales compensation plans
11. Benefits
12. Tax withholding (income tax, social costs, entitlements)
13. Pay raise mandates from the government?  (e.g., Brazil, Colombia, Greece, Malaysia, Turkey)
14. Extra pay (13-month installment)

    a. one time payment

    b. Holiday bonus

    c. Spread across the year

15. Gender equity and reporting
16. How long is it going to take to establish?  (average 6-9 months)

_ii. If Branch then for Corporate_

1. Coordinate with Tax and Finance on appropriate entity formation
2. Coordinate with Employment team on Equity setup

_iii. If Branch then for Product and Privacy_
1. Review employment privacy requirements for direct employees of a subsidiary
2. Review any import/export requirements regarding that the company will have an entity in this location

_iv. If Branch then for Commercial_
1. Review commercial implications of having an entity in this location
2. Any commercial implications or effect on FedRamp for team members in location?

### Steps once approved
1. * [ ] Input into business justification doc if needed
1. * [ ] Inform EPLI carrier of move to new location
2. * [ ] Determine DRI for setting up entity or PEO
3. * [ ] Support Employment Solutions Specialist with informing team members and opening issue for conversion to new employment solution
4. * [ ] Review employment contracts
5. * [ ] Determine if commission plan needs translation or any specific provisions for particular location
6. * [ ] Review applicable employment policies
7. * [ ] Work with Privacy team for any specific updates


## Final Steps in this issue
* [ ] Add label [tbc] and close the issue
