<!--This template should only be used when requesting legal approval to add a new third-party trademark to GitLab. To raise any other issue concerning third-party trademarks with legal, use the general-legal-template issue template instead.-->

### Requestor Details:
* Slack handle: `Slack handle here` 
### Request Details:
<!--
Before creating this issue, search the Third-party Trademark Tracker to check that the logo, and your proposed use, has not already been approved by legal. If it has not, add details of the third-party trademark you are adding to GitLab to the Tracker. The Third-party Trademark Tracker can be accessed from the Third-party Trademark Usage Guidelines which is linked in the Policies section of the GitLab Legal Team handbook page.

If there are any details not already captured in the Third-party Trademark Tracker that you feel are relevant to your request, enter them here.
-->
### Third-party Trademark Tracker Reference
* Indicate the relevant row number in the [Third-party Trademark Tracker](https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit?usp=sharing): `[row number]`.
---
### Checklist
- [ ] **Check the proposed use complies with the Dos and Don’ts**. Legal will not approve requests for proposed use of third-party trademarks that do not comply with the Dos and Don’ts set out in the Third-party Trademark Usage Guidelines.
- [ ] **Consider whether the proposed use constitutes _fair use_**. If you are proposing to add a plain text **wordmark** to GitLab, and the proposed use constitutes _fair use_, legal approval is not required to proceed. The criteria for _fair use_ are set out in the Third-party Trademark Usage Guidelines.
- [ ] Add details of the trademark to the [Third-party Trademark Tracker](https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit?usp=sharing).
- [ ] Identify the relevant row number of the [Third-party Trademark Tracker](https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit?usp=sharing) in the issue description.
- [ ] Add a hyperlink to this issue to the relevant row in the [Third-party Trademark Tracker](https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit?usp=sharing). 
<!-- Choose the appropriate labels from the list below and apply it to the issue. Do not simply note the label within the issue description.--> 

---
### Do Not Edit Below

/confidential
 
/label  ~"New"
 
/assign @dfrhodes
