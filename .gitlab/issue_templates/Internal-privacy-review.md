## Internal Product Privacy Review

_This assessment is a requirement that should be performed as part of anything that involves the collection of new categories of personal data, or a new or different use of personal data that is already being collected. The DRI has the responsibility and accountability for ensuring submission and completion of the Privacy Review. The admin on the project should sign off on the Privacy Review to signify understanding and accountability for the risks in the particular technology. The DRI should ensure that the Privacy and Product Team are consulted in a timely manner, in all issues relating to the protection of personal data. Reach out to #legal if you are not sure whether your project is in scope for this issue._ 

## Requestor to Complete

### Step 1 - Name Issue 

- [ ] Name the issue as follows: Project/Tool Name - Privacy Review - DRI Name

### Step 2 - Link Issues 

<!--(Link the following in the Related Issues" box, not in issue description or comments)-->

- [ ] Security Review issue 
- [ ] Project related Epic

### Step 3 - Indicate which type of review you are requesting

- [ ] First pass Privacy Review *(Note: a first pass review is to inform the Privacy & Product Team of a developing project which is ready for contribution to ensure Privacy By Design and Privacy By Default are appropriately addressed)*
- [ ] Final Privacy Review of internally developed tool
- [ ] Privacy Review for a new integration of a previously approved internally developed tool

### Step 4 - Link Relevant Documentation:

- [ ] Link to any Wiki page with data flow diagrams
- [ ] Link to relevant work trackers
- [ ] Link to previous Internal Privacy Review issues *(Note: this would be for a new integration or for a Final Privacy Review)*

### Step 5 - Answer the Following Questions

- **Where will this project be hosted?**

- **What will the internally developed tool accomplish?** 

- **List all systems with which the tool will integrate** 

- **What systems on the TechStack would this tool replace?**

- **Identify how access will be assigned and controlled**. *(Note: please include relevant information about anticipated credential types, access to testing and production environments, and names of Team Members who would have access)*

- **Mark data subjects**.   *A data subject is an individual, a natural person, whose data can be collected and processed.*
<!--Mark all relevant-->
   - [ ] GitLab team members
   - [ ] Customer Prospects
   - [ ] Free Customers
   - [ ] Paid SaaS Customers
   - [ ] Paid Self-Managed Customers

- **What Personal Data will be used to create any applicable team member accounts?** *(Note: names and email addresses used for log-in are considered Personal Data)*

- **What specific fields of Personal Data will run through the internally developed tool?**
<!--Please list out the specific field names for the data that will run through the tool. For a definition of Personal Data, please review www.about.gitlab.com/handbook/legal/privacy/#privacy-glossary-->

- **Describe the in-tool data deletion capabilities**

- **What is the data retention period established for personal data in this tool?**



## Privacy Officer to Complete

| Risk   | Remediation |
| ------ | ------ |
| describe risk | describe remediation |
| describe risk | describe remediation | 

**Add to Personal Data Request template?**
  - [ ] Yes
  - [ ] No

---

## Approval

- [ ] First Pass Review Approved
- [ ] First Pass Review Not Approved
- [ ] Final Review Approved
- [ ] Not Approved
- [ ] Approved, but further privacy review required upon implementation or as otherwised noted (Notify Business Owner and Lead developer)

<!--Do not edit below this line-->

/confidential

/label ~Privacy 
