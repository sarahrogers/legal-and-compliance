## New Use Privacy Review

_This assessment is a requirement that should be performed before a current tool is used for a new purpose, with additional data categories for processing, or as part of a new integration with another current tool. The Product Manager or the System Admin has the responsibility and accountability for ensuring submission and completion of the Privacy Review.  The admin on the respective technology should sign off on the Privacy Reivew to signify understanding and accountability for the risks in the particular technology.  Product Managers/Admins should ensure that the DPO and Privacy Officer are consulted, in a timely manner, in all issues relating to the protection of personal data. DPOs can delegate DPIA assessments to Security Compliance Analysts where appropriate._

## Requestor to Complete

### Step 1 - Name Issue

- [ ] Name the issue as follows: Software Name - New Use Privacy Review - Business Owner Name

### Step 2 - Link Issues

<!--(Link the following in the "Related Issues" box, not in issue description or in comments)-->

- [ ] Original Privacy Review
- [ ] Privacy Reviews for tools linked in the new integration

### Step 3 - Answer the Following Questions

- **Mark data subjects for the _new_ data**
<!--Mark one or both-->
  - [ ] GitLab team members
  - [ ] Customers

**For new data processing:** 
- **What specific fields of _new_ Personal Data related to the data subjects will be collected, shared, or processed by this tool?** 
<!--List out the specific field names for the data that will be collected/shared/processed. For a definition of "Personal Data," please review [Legal's Privacy Glossary](https://about.gitlab.com/handbook/legal/privacy/#privacy-glossary)-->

**For new integrations:** 
- **What is the reason for the new integration(s)?**
- **What is the proposed data flow?** 

**Where will data be stored?** 
<!--Will data be stored in both/all tools, or will data be deleted from one tool after pulled from the other? etc.-->
---
---

## Privacy Officer to Complete 

| Risk   | Remediation |
| ------ | ------ |
| describe risk | describe remediation |
| describe risk | describe remediation | 

**DPA Required?**
  - [ ]  Yes
  - [ ]  No

**Add to Personal Data Request template?**
  - [ ]  Yes
  - [ ]  No

**Subprocessor?**
  - [ ]  Yes
  - [ ]  No
---
---

## Approval

- [ ]  Yes, with no further review
- [ ]  No
- [ ]  Yes, but further privacy review required upon implementation (Notify Business Owner)

<!--Do not edit below this line-->
-----

/confidential

/label ~"Privacy Vend Rev::Intake"
/label ~Privacy
